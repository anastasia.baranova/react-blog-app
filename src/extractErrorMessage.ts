export const extractErrorMessage = (error: unknown) => {
  if (!error) return undefined;

  if (error instanceof Error) return error.message;

  return 'Unexpected Error';
};
