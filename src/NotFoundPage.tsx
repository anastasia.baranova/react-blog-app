import { Heading, Link, Text } from '@chakra-ui/react';
import { Link as RouterLink } from 'react-router-dom';

export const NotFoundPage: React.FC = () => (
  <div className="not-found">
    <Heading>Sorry</Heading>
    <Text>That page cannot be found</Text>
    <Link as={RouterLink} to="/">
      Back to the homepage...
    </Link>
  </div>
);
