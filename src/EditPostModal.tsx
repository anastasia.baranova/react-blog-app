import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  VStack,
  FormControl,
  FormLabel,
  Input,
  Flex,
  Spacer,
  Textarea,
  ModalFooter,
  Button,
  Text,
} from '@chakra-ui/react';
import omit from 'lodash/omit';
import { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useQueryClient, useMutation } from 'react-query';

import { TPost } from './TPost';
import { MAX_POST_BODY_LENGTH } from './constants';
import { updatePost } from './postsApi';

type TEditPostModalProps = {
  post: TPost;
  onClose: () => void;
  isOpen: boolean;
};

export const EditPostModal: React.FC<TEditPostModalProps> = ({ post, onClose, isOpen }) => {
  const {
    register,
    // eslint-disable-next-line id-denylist
    handleSubmit,
    watch,
    formState: { errors },
    reset,
  } = useForm<Pick<TPost, 'body' | 'title'>>({
    mode: 'onBlur',
    defaultValues: {
      body: '',
      title: '',
    },
  });

  useEffect(() => {
    reset(omit(post, 'id'));
  }, [post, reset]);

  const queryClient = useQueryClient();

  const updatePostMutation = useMutation(async (postFormValues: Pick<TPost, 'body' | 'title'>) => updatePost(post.id, postFormValues), {
    onSuccess: (updatedPost) => {
      queryClient.setQueryData(['posts', updatedPost.id], updatedPost);
      void queryClient.invalidateQueries('posts', { exact: true });
      queryClient.setQueryData<TPost[] | undefined>('posts', (oldPosts) =>
        oldPosts?.map((oldPost) => (oldPost.id === updatedPost.id ? updatedPost : oldPost))
      );

      onClose();
    },
  });

  const submitForm = handleSubmit((updatedPost) => {
    updatePostMutation.mutate(updatedPost);
  });

  const postBodyLength = watch('body').length;

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Edit Post</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <VStack as="form" align="stretch" px={4} onSubmit={submitForm}>
            <FormControl>
              <FormLabel>Post title</FormLabel>
              <Input {...register('title', { required: true })} isInvalid={!!errors.title} />
              {errors.title?.type === 'required' && <Text color="red.500">This field is required</Text>}
            </FormControl>
            <FormControl>
              <Flex>
                <FormLabel>Post text</FormLabel>
                <Spacer />
                <Text color={postBodyLength > MAX_POST_BODY_LENGTH ? 'red.500' : undefined}>
                  {postBodyLength}/{MAX_POST_BODY_LENGTH}
                </Text>
              </Flex>
              <Textarea {...register('body', { required: true, maxLength: MAX_POST_BODY_LENGTH })} isInvalid={!!errors.body}></Textarea>
              {errors.body?.type === 'required' && <Text color="red.500">This field is required</Text>}
              {errors.body?.type === 'maxLength' && <Text color="red.500">Post text cannot exceed {MAX_POST_BODY_LENGTH} characters</Text>}
            </FormControl>
          </VStack>
        </ModalBody>
        <ModalFooter>
          <Button mr={4} onClick={submitForm}>
            Save
          </Button>
          <Button variant="ghost" onClick={onClose}>
            Cancel
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};
