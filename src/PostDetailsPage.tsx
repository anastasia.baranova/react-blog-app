import { Box, Button, Heading, HStack, Text, useDisclosure } from '@chakra-ui/react';
import omit from 'lodash/omit';
import { useMutation, useQuery, useQueryClient } from 'react-query';
import { useNavigate, useParams } from 'react-router-dom';

import { EditPostModal } from './EditPostModal';
import { TPost } from './TPost';
import { extractErrorMessage } from './extractErrorMessage';
import { deletePost, getPost } from './postsApi';

export const PostDetailsPage: React.FC = () => {
  const navigate = useNavigate();
  const { id } = useParams();
  const {
    data: post,
    error,
    isLoading,
    isError,
  } = useQuery(
    ['posts', id],
    async ({ signal }) => {
      if (!id) throw new Error('Post ID is missing');

      return getPost(id, { signal });
    },
    { staleTime: 5 * 60 * 1000 }
  );

  const queryClient = useQueryClient();

  const deletePostMutation = useMutation(deletePost, {
    onSuccess: () => {
      void queryClient.invalidateQueries('posts', { exact: true });
      void queryClient.invalidateQueries(['posts', Number(id)]);
      queryClient.setQueryData<TPost[] | undefined>('posts', (oldPosts) => oldPosts?.filter((p) => p.id !== id));
      navigate('/');
    },
  });

  const { isOpen: isEditModalOpen, onClose: closeEditModal, onOpen: openEditModal } = useDisclosure();

  if (isLoading) return <div>Loading...</div>;

  if (isError) return <div>{extractErrorMessage(error)}</div>;

  if (post)
    return (
      <Box as="article">
        <Heading size="lg" mb={4} variant="colored">
          {post.title}
        </Heading>
        <Text mb={4} align="end">
          Written by{' '}
          <Text as="span" color="purple.500" fontWeight="bold">
            {post.author.name}
          </Text>
        </Text>
        <Text mb={4}>{post.body}</Text>
        <HStack mb={6} spacing={4}>
          <Button
            colorScheme="red"
            onClick={() => {
              if (!id) throw new Error('Post ID is missing');

              deletePostMutation.mutate(id);
            }}
          >
            Delete Post
          </Button>
          <Button onClick={openEditModal}>Edit Post</Button>
          <EditPostModal post={omit(post, 'author')} isOpen={isEditModalOpen} onClose={closeEditModal} />
        </HStack>
      </Box>
    );

  throw new Error('Unexpected case: post is missing');
};
