import { extendTheme, theme as base, withDefaultColorScheme } from '@chakra-ui/react';

export const theme = extendTheme(
  {
    fonts: {
      heading: `Quicksand, ${base.fonts.heading}`,
      body: `Quicksand, ${base.fonts.heading}`,
    },
    components: {
      Heading: {
        variants: {
          colored: {
            color: 'purple.500',
          },
        },
      },
      Link: {
        baseStyle: {
          _hover: {
            color: 'purple.500',
            textDecoration: 'none',
          },
        },
      },
    },
  },
  withDefaultColorScheme({ colorScheme: 'purple' })
);
